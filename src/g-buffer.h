#ifndef G_BUFFER_H
#define G_BUFFER_H

#include <OpenGL/gl3.h>

class GBuffer {
public:
	GBuffer(unsigned int width, unsigned height);
	~GBuffer();

	GLuint frame_buffer;
	GLuint positions_buffer; // RGB32F (i.e. vec3) render target
	GLuint normals_buffer;   // RGB32F (i.e. vec3) render target
private:
	GBuffer();
	GBuffer(const GBuffer& original);
	GBuffer& operator=(const GBuffer& original);
};


#endif /* end of include guard: G_B */
