//------------------------------------------------------------------------------
//
//  g-buffer.cpp
//  01
//
//  Created by Arno in Wolde Luebke on 15.09.14.
//  Copyright (c) 2014 Arno in Wolde Luebke. All rights reserved.
//
//------------------------------------------------------------------------------
#include "g-buffer.h"
#include <cassert>
//------------------------------------------------------------------------------
GBuffer::GBuffer(unsigned int width, unsigned int height)
{
	glGenFramebuffers(1, &this->frame_buffer);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->frame_buffer);
	glGenRenderbuffers(1, &this->positions_buffer);
	glBindRenderbuffer(GL_RENDERBUFFER, this->positions_buffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB32F, width, height);
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_RENDERBUFFER, this->positions_buffer);
	glGenRenderbuffers(1, &this->normals_buffer);
	glBindRenderbuffer(GL_RENDERBUFFER, this->normals_buffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB32F, width, height);
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
		GL_RENDERBUFFER, this->normals_buffer);
	GLenum targets[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
	glDrawBuffers(2, targets);
	
	assert(GL_FRAMEBUFFER_COMPLETE == glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER));
}
//------------------------------------------------------------------------------
GBuffer::~GBuffer()
{
	glDeleteBuffers(1, &this->frame_buffer);
	glDeleteBuffers(1, &this->normals_buffer);
	glDeleteFramebuffers(1, &this->frame_buffer);
}
//------------------------------------------------------------------------------
