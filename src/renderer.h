#ifndef RENDERER_H
#define RENDERER_H

#include <obj/file.h>

class Renderer {
	enum {
		SUCCESS = 0
	};

public:
	/// Explicitly creates the renderer
	/// @return The renderer on success, NULL on failure.
	static Renderer* create();
	
	// Explicitly destroys the object
	void destroy();
	
	/// Sets the scene to be rendered
	/// @param scene .obj class that holds the scene.
	int setScene(const obj::File& scene);
	
	/// Renders the scene.
	/// @return SUCCESS on success, an error code otherwise
	int render();
	
	
private:
	Renderer();
	virtual ~Renderer ();
};


#endif /* end of include guard: RENDERER_H */
