//------------------------------------------------------------------------------
//
//  scene-geometry.cpp
//  001
//
//  Created by Arno in Wolde Luebke on 14.09.14.
//  Copyright (c) 2014 Arno in Wolde Luebke. All rights reserved.
//
//------------------------------------------------------------------------------
#include "scene-geometry.h"
//------------------------------------------------------------------------------
struct Converter {
	const obj::File* file;
	obj::Array<GLfloat> positions;
	obj::Array<GLfloat> normals;
	//obj::Array<GLuint> indices;
	SceneGeometry::Node* root;
	
	Converter(const obj::File& scene);
	~Converter();
	void load(SceneGeometry::Node& node, const obj::Node& obj_node);
	void copyf(obj::Array<float>& dst, const obj::Array<float>& src,
		size_t dst_index, size_t src_index, size_t num_elements);
	void push3(obj::Array<float>& dst, const obj::Array<float>& src,
		size_t idx);
};
//------------------------------------------------------------------------------
Converter::Converter(const obj::File& scene) : file(&scene)
{
	assert(scene.getNormals()->getCount() > 0);

	this->root = new SceneGeometry::Node();
	this->load(*this->root, *scene.getRoot());
}
//------------------------------------------------------------------------------
Converter::~Converter()
{
	// NOTE: root is not deleted, since SceneGeometry holds onto it.
	//       root is deleted in SceneGeometry's destructor.
}
//------------------------------------------------------------------------------
void Converter::load(SceneGeometry::Node& node, const obj::Node& obj_node)
{
	const obj::Array<int>* pids = obj_node.getPositionIDs();
	const obj::Array<int>* nids = obj_node.getNormalIDs();
//	node.index = this->indices.getCount();
	node.index = this->positions.getCount();
	node.num_vertices = pids->getCount();
	strcpy(node.name, obj_node.getName());
	
	for (size_t i = 0; i < pids->getCount(); i++) {
		this->push3(this->positions, *this->file->getPositions(),
			pids->get(i));
		this->push3(this->normals, *this->file->getNormals(),
			nids->get(i));
//		this->copyf(*this->positions, *this->file->getPositions(),
//			pids->get(i), pids->get(i), 3);
//		this->copyf(*this->normals, *this->file->getNormals(),
//			pids->get(i), nids->get(i), 3);
//		this->indices.push((GLuint)pids->get(i));
	}
	
	const obj::Array<const obj::Node*>* children = obj_node.getChildren();
	
	for (size_t i = 0; i < children->getCount(); i++) {
		SceneGeometry::Node* n = new SceneGeometry::Node();
		node.children.push(n);
		load(*n, *children->get(i));
	}
}
//------------------------------------------------------------------------------
void Converter::push3(obj::Array<float> &dst, const obj::Array<float> &src,
	size_t idx)
{
	for (size_t i = 0; i < 3; i++) {
		dst.push(src.get(3 * idx + i));
	}
}
//------------------------------------------------------------------------------
void Converter::copyf(obj::Array<float>& dst, const obj::Array<float>& src,
		size_t dst_index, size_t src_index, size_t num_elements)
{
	for (size_t i = 0; i < num_elements; i++) {
		dst.get(num_elements * dst_index + i) =
			src.get(num_elements * src_index + i);
	}
}
//------------------------------------------------------------------------------
//	Node utilities
//------------------------------------------------------------------------------
static void destroyNode(SceneGeometry::Node& node)
{
	// destroyes a SceneGeometry::Node and its children
	
	for (size_t i = 0; i < node.children.getCount(); i++) {
		destroyNode(*node.children.get(i));
	}

	delete &node;
}
//------------------------------------------------------------------------------
//	Scene Geometry
//------------------------------------------------------------------------------
SceneGeometry::SceneGeometry(const obj::File& scene)
{
	Converter c(scene);
	//this->index_buffer_size = c.indices.getSize();
	this->attribute_buffer_size = c.positions.getSize();
	this->num_vertices = c.positions.getCount() / 3;
	//this->num_faces = c.indices.getCount() / 3;
	this->num_faces = this->num_vertices / 3;
	this->root = c.root;
	glGenVertexArrays(1, &this->vertext_array);
	glBindVertexArray(this->vertext_array);
	
	// positions
	glGenBuffers(1, &this->positions_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, this->positions_buffer);
	glBufferData(GL_ARRAY_BUFFER, c.positions.getSize(),
		c.positions.getData(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// normals
	glGenBuffers(1, &this->normals_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, this->normals_buffer);
	glBufferData(GL_ARRAY_BUFFER, c.normals.getSize(),
		c.normals.getData(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// indices
//	glGenBuffers(1, &this->index_buffer);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->index_buffer);
//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, c.indices.getSize(),
//		c.indices.getData(), GL_STATIC_DRAW);
	assert(GL_NO_ERROR == glGetError());
}
//------------------------------------------------------------------------------
SceneGeometry::~SceneGeometry()
{
	destroyNode(*this->root);
	glDeleteBuffers(1, &this->positions_buffer);
	glDeleteBuffers(1, &this->normals_buffer);
	//glDeleteBuffers(1, &this->index_buffer);
	glDeleteVertexArrays(1, &this->vertext_array);
}
//------------------------------------------------------------------------------
