#ifndef SCENE_GEOMETRY_H
#define SCENE_GEOMETRY_H

#include <OpenGL/gl3.h>
#include <obj/file.h>
#include "common.h"

struct SceneGeometry {

	struct Node {
		String name;
		size_t index;
		size_t num_vertices;
		obj::Array<Node*> children;
	};

	SceneGeometry(const obj::File& scene);
	~SceneGeometry();
	
	Node* root;
	GLuint vertext_array;
//	GLuint index_buffer;
	GLuint positions_buffer;
	GLuint normals_buffer;
	size_t num_faces;
	size_t num_vertices;
	size_t attribute_buffer_size;
//	size_t index_buffer_size;
	
private:
	SceneGeometry(const SceneGeometry& original);
	SceneGeometry& operator=(const SceneGeometry& original);
};

#endif /* end of include guard: SCENE-GEOMETRY_H */
