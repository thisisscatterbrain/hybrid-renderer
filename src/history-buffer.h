#ifndef HISTORY_BUFFER_H
#define HISTORY_BUFFER_H

#include "frame.h"

class HistoryBuffer {
public:
	HistoryBuffer(unsigned int width, unsigned int height,
		unsigned int max_frames);
	~HistoryBuffer();

	void advance() {this->current_frame = (this->current_frame + 1)
		% this->max_frames;}
	Frame* getCurrentFrame() {return this->frames[current_frame];};
	
private:
	Frame** frames;
	unsigned int current_frame;
	unsigned int max_frames;
};

#endif /* end of include guard: HISTORY-BUFFER_H */
