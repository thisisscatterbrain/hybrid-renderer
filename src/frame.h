#ifndef FRAME_H
#define FRAME_H

#include "g-buffer.h"

class Frame {
public:
	Frame(unsigned int width, unsigned int height);
	virtual ~Frame () {};

	GBuffer g_buffer;
};

#endif /* end of include guard: FRAME_H */
