#ifndef RASTERIZER_H
#define RASTERIZER_H

#include "scene-geometry.h"
#include <cgm/matrix4.h>

class Rasterizer {
public:
	Rasterizer(SceneGeometry& scene, const cgm::Matrix4f& perspective);
	void setWorldToCamera(const cgm::Matrix4f& world_to_camera);
	~Rasterizer();
	void rasterize();
	
private:
	Rasterizer(const Rasterizer& original);
	Rasterizer& operator=(const Rasterizer& original);

	GLuint program;
	const SceneGeometry* scene;
	cgm::Matrix4f perspective;
	cgm::Matrix4f world_to_camera;
};

#endif /* end of include guard: RASTERIZER_H */
