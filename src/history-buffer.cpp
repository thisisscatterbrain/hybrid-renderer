//------------------------------------------------------------------------------
//
//  history-buffer.cpp
//  01
//
//  Created by Arno in Wolde Luebke on 15.09.14.
//  Copyright (c) 2014 Arno in Wolde Luebke. All rights reserved.
//
//------------------------------------------------------------------------------
#include "history-buffer.h"
//------------------------------------------------------------------------------
HistoryBuffer::HistoryBuffer(unsigned int width, unsigned int height,
	unsigned int max_frames) : frames(0), current_frame(0),
	max_frames(max_frames)
{
	this->frames = new Frame*[max_frames];
	
	for (unsigned int i = 0; i < max_frames; ) {
		this->frames[i] = new Frame(width, height);
	}
}
//------------------------------------------------------------------------------
HistoryBuffer::~HistoryBuffer()
{
	for (unsigned int i = 0; i < max_frames; ) {
		delete this->frames[i];
	}
}
//------------------------------------------------------------------------------