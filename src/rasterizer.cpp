//
//  rasterizer.cpp
//  001
//
//  Created by Arno in Wolde Luebke on 14.09.14.
//  Copyright (c) 2014 Arno in Wolde Luebke. All rights reserved.
//
//------------------------------------------------------------------------------
#include "common.h"
#include "rasterizer.h"
#include <cgm/matrix4-transforms.h>
#include <glue/program.h>
//------------------------------------------------------------------------------
#define TO_STRING(x) #x
//------------------------------------------------------------------------------
//	Shader
//------------------------------------------------------------------------------
static const char* vertex_shader =
	"#version 410\n"
TO_STRING(
	uniform mat4 perspective;
	uniform mat4 view;
	
	in vec3 in_position;
	in vec3 in_normal;
	
	out vec3 position;
	out vec3 normal;
	
	void main()
	{
		position = in_position;
		normal = in_normal;
		gl_Position = perspective * view * vec4(in_position, 1.0);
	}
);
//------------------------------------------------------------------------------
static const char* fragment_shader =
	"#version 410\n"
TO_STRING(
	in vec3 position;
	in vec3 normal;
	
	out vec4 out_fragment_color;
	
	void main()
	{
		out_fragment_color = vec4(normal, 1.0);
	}
);
//------------------------------------------------------------------------------
//	Rasterizer
//------------------------------------------------------------------------------
Rasterizer::Rasterizer(SceneGeometry& scene, const cgm::Matrix4f& perspective) :
	scene(&scene), perspective(perspective),
	world_to_camera(cgm::makeIdentity<float>())
{
	this->program = glCreateProgram();
	glueProgramAttachShaderWithSource(this->program, GL_VERTEX_SHADER,
		vertex_shader);
	glueProgramAttachShaderWithSource(this->program, GL_FRAGMENT_SHADER,
		fragment_shader);
	glBindAttribLocation(this->program, 0, "in_position");
	glBindAttribLocation(this->program, 1, "in_normal");
	glBindFragDataLocation(this->program, 0, "out_fragment_color");
	glueProgramLink(this->program);
	glUseProgram(this->program);
	glueProgramUniformMatrix4(this->program, "perspective",
		this->perspective, GL_TRUE);
	glueProgramUniformMatrix4(this->program, "view", this->world_to_camera,
		GL_TRUE);
	assert(GL_NO_ERROR == glGetError());
	assert(glIsEnabled(GL_DEPTH_TEST));
}
//------------------------------------------------------------------------------
Rasterizer::~Rasterizer()
{
	glDeleteProgram(this->program);
}
//------------------------------------------------------------------------------
void Rasterizer::setWorldToCamera(const cgm::Matrix4f& world_to_camera)
{
	this->world_to_camera = world_to_camera;
	glueProgramUniformMatrix4(this->program, "view", this->world_to_camera,
		GL_TRUE);
	glUseProgram(this->program);
}
//------------------------------------------------------------------------------
void Rasterizer::rasterize()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(this->program);
	glBindVertexArray(this->scene->vertext_array);
	glDrawArrays(GL_TRIANGLES, 0, (GLsizei)this->scene->num_vertices);
	glFlush();
}
//------------------------------------------------------------------------------


