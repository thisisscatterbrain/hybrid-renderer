#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cassert>
#include <cstdio>
#include <cgm/matrix4-transforms.h>
#include "scene-geometry.h"
#include "rasterizer.h"
#include "g-buffer.h"

int main(void)
{
	GLFWwindow* window;
		
	/* Initialize the library */
	if (!glfwInit()) {
		return -1;
	}
	
	/* Set window hints here */
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		
	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(1280, 720, "Hello World", NULL, NULL);
	
	if (!window) {
		glfwTerminate();
		return -1;
	}
	
	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	/* initialize glew */
	glewExperimental = GL_TRUE;
	assert(GLEW_OK == glewInit());
	glGetError();

	/* some gl settings */
	glClearColor(0.7, 0.7, 0.7, 1.0);
	glEnable(GL_DEPTH_TEST);

	obj::File* ofp = obj::File::create("../obj-files/powerplant.obj");
	SceneGeometry sg(*ofp);
	cgm::Matrix4f p = cgm::makePerspective<float>(1.4, 1280.0 / 720.0, 0.001, 100.0);
	cgm::Matrix4f w2c = cgm::makeWorldToCamera(cgm::Vector3f(0.0, 0.0, -3.0),
		cgm::Vector3f(0.0, 0.0, 0.0), cgm::Vector3f(0.0, 1.0, 0.0));
	Rasterizer r(sg, p);
	r.setWorldToCamera(w2c);
	
	GBuffer buffer(1280, 720);
	GBuffer buffer2(1280, 720);
	GBuffer buffer3(1280, 720);
	
	printf("%s\n", glGetString(GL_VERSION));
	
	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window)) {
		/* Render here */
		r.rasterize();
		
		/* Swap front and back buffers */
		glfwSwapBuffers(window);
		
		/* Poll for and process events */
		glfwPollEvents();
	}
	
	glfwTerminate();
	return 0;
}