A Realtime Hybrid Renderer
==========================
This project is an attempt to write a renderer that relies on rasterization as 
well as ray tracing to create real-time cg. It is mostly thought as an excersice
for me to motivate myself more to learn recent real time cg technologies (OpenGL,
CUDA, OptiX, OpenCL) and classic CG algorithms (ray tracing, ambient occlusion,
soft shadows, reflaction/refraction, ...). 

(Final) Requirements
--------------------
* intended for 720p
* 50k - 100k triangles per scene
* fully dynamic
* direct lighting (obvious ...)
* anti aliasing
* (dynamic) ambient occlusion
* soft shadows
* reflaction & refraction
* 30+ fps

A first design:
---------------
The rasterizer pipeline is used to greate a G-buffer employing OpenGL. The 
G-buffer is used by a ray tracing engine to create the final picture. The
first iteration of the ray tracing engine is supposed to be implemented in
OptiX and serve as a prototype. Later a implementation on CUDA is planned
for optimization reason, as CUDA gives more liberty. Ultimately the engine
is planned to be ported to OpenCL for portability reasons. 

References
----------
* A Really Realtime Raytracer, Matt Swoboda: http://www.ustream.tv/recorded/45360291
  *Basically, I try to reimplement this renderer*
* Pixel-Correct Shadow Maps with Temporal Reprojection and Shadow Test Confidence., Scherzer et. al
http://www.researchgate.net/publication/220852965_Pixel-Correct_Shadow_Maps_with_Temporal_Reprojection_and_Shadow_Test_Confidence
*Explains the temporal reprojection technique, that recently has been used 
in many cases (e.g. as replacement for "upscaling", ambient occlusionm or
anti aliasing)*
* Killzone Shadow Fall Resolution – Temporal Reprojection & Shadow Fall 1080P,
http://www.redgamingtech.com/killzone-shadow-fall-resolution-temporal-reprojection-shadow-fall-1080p/
*A explanation on how Killzone uses temporal reprojection*

